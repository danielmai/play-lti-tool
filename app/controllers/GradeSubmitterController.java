package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import play.Logger;
import play.libs.ws.WSClient;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Iterator;

/**
 * Created by Daniel on 4/3/16.
 */
public class GradeSubmitterController extends Controller {

    private final WSClient ws;

    @Inject
    public GradeSubmitterController(WSClient ws) {
        this.ws = ws;
    }

    public Result submitGradeToCanvas() {
        Http.Cookie outcomeServiceUrlCookie = request().cookie("lis_outcome_service_url");
        Http.Cookie sourcedIdCookie = request().cookie("lis_result_sourcedid");
        if (outcomeServiceUrlCookie == null) {
            Logger.info("lis_outcome_service_url cookie not found.");
            return badRequest();
        }
        if (sourcedIdCookie == null) {
            Logger.info("lis_result_sourcedid cookie not found.");
            return badRequest();
        }

        String outcomeServiceUrl = outcomeServiceUrlCookie.value();
        String sourcedId = sourcedIdCookie.value();
        if (outcomeServiceUrl == null
                || outcomeServiceUrl.equals("")
                || sourcedId == null
                || sourcedId.equals("")) {
            return badRequest();
        }

        Logger.info("lis_outcome_service_url = {}", outcomeServiceUrl);
        Logger.info("lis_result_sourcedid = {}", sourcedId);

        // Example:
        // [
        //   {"correct":0,"errors":0,"maxscore":4,"activity":"ebook-bjeo-6-ch16-sec01-int1-1","element":"horstmann_objectdiagram_1"},
        //   {"correct":0,"errors":0,"maxscore":2,"activity":"ebook-bjeo-6-ch16-sec01-int1-2","element":"horstmann_objectdiagram_1"}
        // ]
        JsonNode jsonPayload = request().body().asJson();
        Logger.info("json from client = {}", jsonPayload);

        // Add all the 'correct' and 'maxscore' values to get the total score
        int correct = 0;
        int maxScore = 0;
        Iterator<JsonNode> nodeIterator = jsonPayload.elements();
        while (nodeIterator.hasNext()) {
            JsonNode exercise = nodeIterator.next();
            Logger.info(exercise.toString());
            correct = correct + exercise.get("correct").asInt();
            maxScore = maxScore + exercise.get("maxscore").asInt();
        }

        double score = (double) correct / maxScore;

        Logger.info("correct = {}, maxScore = {}, score = {}", correct, maxScore, score);
        Logger.info(views.xml.score_passback.render(sourcedId, score).toString());

        try {
            passbackGradeToCanvas(outcomeServiceUrl, views.xml.score_passback.render(sourcedId, score).toString(),
                    "fred", "fred");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OAuthMessageSignerException e) {
            e.printStackTrace();
        } catch (OAuthExpectationFailedException e) {
            e.printStackTrace();
        } catch (OAuthCommunicationException e) {
            e.printStackTrace();
        }
        return ok();
    }

    /**
     * Pass back the grade to Canvas. If the <code>xml</code> is set
     * up with fetch URL string, then also pass back the URL to the
     * codecheck report.
     * @param gradePassbackURL the grade passback URL from the LTI launch
     * @param xml the data to send off, with the sourcedId, score, and possibly the fetchURL
     * @param oauthKey the oauth consumer key
     * @param oauthSecret the oauth secret key
     */
    public static void passbackGradeToCanvas(String gradePassbackURL, String xml,
                                               String oauthKey, String oauthSecret)
            throws URISyntaxException, IOException,
            OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException
    {
        // Now post the score to Canvas. Make sure to sign the POST correctly
        // with OAuth 1.0, including the digest of the XML body. Also make sure
        // to set the content-type to application/xml.

        // Create an oauth consumer in order to sign the grade that will be sent.
        DefaultOAuthConsumer consumer = new DefaultOAuthConsumer(oauthKey, oauthSecret);

        // empty token key and token secret, because they are not
        // needed to sign the request that will pass back the grade.
        // This is how the Ruby OAuth code works too.
        consumer.setTokenWithSecret("", "");

        // This is the URL that we will send the grade to so it can go back to Canvas
        URL url = new URL(gradePassbackURL);

        // This is the part where we send the HTTP request
        HttpURLConnection request = (HttpURLConnection) url.openConnection();

        // Set http request to POST
        request.setRequestMethod("POST");
        request.setDoOutput(true);

        // Set the content type to accept xml
        request.setRequestProperty("Content-Type", "application/xml");

        // Set the content-length to be the length of the xml
        request.setRequestProperty("Content-Length", Integer.toString(xml.length()));

        // Sign the request per the oauth 1.0 spec
        consumer.sign(request); // Throws
                                // OAuthMessageSignerException,
                                // OAuthExpectationFailedException,
                                // OAuthCommunicationException

        // POST the xml to the grade passback url
        request.getOutputStream().write(xml.getBytes("UTF8"));

        // send the request
        request.connect();

        request.getInputStream(); // This is needed, or else the grade won't get passed back.
                                  // It doesn't work when I simply call the connect() method.
                                  // But getInputStream() works. (Don't know why.)
    }
}
