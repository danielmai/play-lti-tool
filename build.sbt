name := """play-lti-tool"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "oauth.signpost" % "oauth-signpost" % "1.2.1.2"
)


fork in run := true