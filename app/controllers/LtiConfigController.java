package controllers;

import play.Logger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class LtiConfigController extends Controller {

    public Result config() throws UnknownHostException {
        Http.Request request = request();

        String domain = controllers.routes.HomeController.index().absoluteURL(request, true);
        String launchUrl = controllers.routes.LtiConfigController.setupExercises().absoluteURL(request, true);
        String resourceSelectionUrl = controllers.routes.LtiConfigController.chooseExercises().absoluteURL(request, true);

        return ok(views.xml.lti_config
                .render("Interactive Exercises",
                        domain,
                        launchUrl,
                        resourceSelectionUrl))
                .as("application/xml");
    }

    public Result chooseExercises() {
        Map<String, String[]> postParams = request().body().asFormUrlEncoded();
        return ok(views.html.choose_problem.render(postParams.get("launch_presentation_return_url")[0]));
    }

    /**
     * POST method
     */
    public Result setupExercises() throws UnsupportedEncodingException {
        Map<String, String[]> postParams = request().body().asFormUrlEncoded();

        if (postParams.get("lis_outcome_service_url") == null || postParams.get("lis_result_sourcedid") == null) {
            flash("warning", "");
        } else {
            response().setCookie(new Http.Cookie("lis_outcome_service_url", postParams.get("lis_outcome_service_url")[0],
                    null, null, null, false, false));
            response().setCookie(new Http.Cookie("lis_result_sourcedid", postParams.get("lis_result_sourcedid")[0],
                    null, null, null, false, false));
        }
        String url = controllers.routes.LtiConfigController.exercises().url()
                + "?urls=" + URLEncoder.encode(request().getQueryString("urls"), "UTF-8");
        Logger.info(url);
        return redirect(url);
    }

    /**
     * GET method
     */
    public Result exercises() {
        String exercises = request().getQueryString("urls");
        Logger.info(exercises);
        List<String> exercisesList = Arrays.asList(exercises.split("\n"));
        return ok(views.html.exercises.render(exercisesList));
        //String c = request().cookie("lis_outcome_service_url").value();
        //String c2 = request().cookie("lis_result_sourcedid").value();
        //return ok(views.html.exercises.render()).as("text/plain");
    }
}
